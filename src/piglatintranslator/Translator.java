package piglatintranslator;

public class Translator {
	
	public static final String NIL = "nil";
	public static final String NOT_ALLOWED = "Phrase not allowed!";
	private String phrase;
	private String startingLetter;
	private String secondLetter;
	private String[] splittedPhrase;
	private String[] translatedPhrase;
	private String[] splittedWord;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		String originalPhrase = phrase;
		//CHECK IF THE PHRASE IS EMPTY
		if(phrase.isEmpty()) {
			return NIL;
		}
		//VECTOR CONTAINING FOR EVERY CELL A WORD OF THE PHRASE TO TRANSLATE IT ONE BY ONE
		splittedPhrase = phrase.split("[^\\w]+");
		translatedPhrase = new String[splittedPhrase.length];
		
		startingLetter = String.valueOf(phrase.charAt(0));
		splittedWord = phrase.split("");
		if(!isStartingLetterUpperCase()) {
			for(int i = 1; i < splittedWord.length; i++) {
				if(Character.isUpperCase(splittedWord[i].charAt(0))) {
					return NOT_ALLOWED;
				}
			}
		}
		//TRANSLATION
		for(int i = 0; i < splittedPhrase.length; i++) {
			phrase = splittedPhrase[i];
			phrase = phrase.toLowerCase();
			
			if(startWithVowel()) {
				if(phrase.endsWith("y")) {
					phrase = phrase + "nay";
				}else if(endWithVowel()) {
					phrase = phrase + "yay";
				}else if(!endWithVowel()) {
					phrase = phrase + "ay";
				}
			}else if(!startWithVowel()) {
				startingLetter = String.valueOf(phrase.charAt(0));
				secondLetter = String.valueOf(phrase.charAt(1));
				
				if(!isAVowel()) {
					phrase = phrase.substring(2) + startingLetter + secondLetter + "ay";
				}else {
					phrase = phrase.substring(1) + startingLetter + "ay";
				}
			}
			translatedPhrase[i] = phrase;
		}
		//CHECK IF THE ORIGINAL PHRASE IS ALL UPPER CASE SO IT CAN TURN ALL UPPER CASE THE TRANSLATED ONE
		if(isStartingLetterUpperCase()) {
			if(isRestOfThePhraseAllUpperCase()) {
				for(int i = 0; i < splittedPhrase.length; i++) {
					translatedPhrase[i] = translatedPhrase[i].toUpperCase();
				}
			}
		}
		//USE A STRING BUILDER TO MANAGE PHRASES CONTAINING MORE WORDS SEPARATED BY DASH OR WHITESPACE
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < translatedPhrase.length; i++) {
		    stringBuilder.append(translatedPhrase[i]);
		    if(i < (translatedPhrase.length-1)) {
		    	if(originalPhrase.contains("-")) {
		    		stringBuilder.append("-");
		    	}else {
		    		stringBuilder.append(" ");
		    	}	
		    }
		    if(i == (translatedPhrase.length-1)) {
		    	if(originalPhrase.contains("!")) {
		    		stringBuilder.append("!");
		    	}
		    }
		}
		phrase = stringBuilder.toString();
		//CHECK IF THE FIRST LETTER OF THE ORIGINAL PHRASE IS UPPER CASE AND THEN CHANGE THE FIRST LETTER OF THE TRANSLATED PHRASE FROM LOWER TO UPPER CASE
		startingLetter = String.valueOf(originalPhrase.charAt(0));
		if(isStartingLetterUpperCase() && isRestOfThePhraseAllLowerCase()) {
			String firstWord = phrase.toLowerCase();
			phrase = firstWord.substring(0,1).toUpperCase() + firstWord.substring(1);
		}
		return phrase;
	}
	
	private boolean startWithVowel() {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"));
	}
	
	private boolean endWithVowel() {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private boolean isAVowel() {
		return (secondLetter.equals("a") || secondLetter.equals("e") || secondLetter.equals("i") || secondLetter.equals("o") || secondLetter.equals("u"));
	}
	
	private boolean isStartingLetterUpperCase() {
		return Character.isUpperCase(startingLetter.charAt(0));
	}
	
	private boolean isRestOfThePhraseAllUpperCase() {
		for(int i = 1; i < splittedWord.length; i++) {
			if(!(Character.isUpperCase(splittedWord[i].charAt(0)))) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isRestOfThePhraseAllLowerCase() {
		for(int i = 1; i < splittedWord.length; i++) {
			if(!(Character.isLowerCase(splittedWord[i].charAt(0)))) {
				return false;
			}
		}
		return true;
	}
}
