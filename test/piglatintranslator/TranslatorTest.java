package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyInputPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("anynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("utilitynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("appleyay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("askay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedBySpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedByDash() {
		String inputPhrase = "hello-world";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay-orldway", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!", translator.translate());
	}

	@Test
	public void testTranslationPhraseWithUpperCaseWordsAndNotAllowed() {
		String inputPhrase = "biRd";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(Translator.NOT_ALLOWED, translator.translate());
	}	
	
	@Test
	public void testTranslationPhraseWithAllUpperCaseWords() {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("APPLEYAY", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithFirstLetterUpperCaseOtherLowerCase() {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("Ellohay", translator.translate());
	}	
}
